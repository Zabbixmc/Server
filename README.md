Права доступа
Информация, связанная с разрешениями.
Чтобы дать игроку разрешение, используйте: /lp user {Ник} permission set {разрешение}

Косметика и права

Теги и права
VIP ❀ - deluxetags.tag.vip
EVP ❀ - deluxetags.tag.evp
MVP ❀ - deluxetags.tag.mvp

➲ - deluxetags.tag.arrow

✤ - deluxetags.tag.clover

✺ - deluxetags.tag.fireworks

❃ - deluxetags.tag.flower

☹ - deluxetags.tag.frown

❤ - deluxetags.tag.heart

♬ - deluxetags.tag.music

➊ - deluxetags.tag.one

☮ - deluxetags.tag.peace

✎ - deluxetags.tag.pencil

☣ - deluxetags.tag.radioactive

♨ - deluxetags.tag.coffee

☠ - deluxetags.tag.skull

☺ - deluxetags.tag.smile

☃ - deluxetags.tag.snowman

✪ - deluxetags.tag.star

❂ - deluxetags.tag.sun

☯ - deluxetags.tag.yinyang

*************************************************************
Следы из частиц и права
Angry - trail.angry
Breath - trail.breath
Color - trail.colors
Damage - trail.damage
Ender - trail.ender
Enchant - trail.enchant
Endrod - trail.endrod
Flame - trail.flame
Green - trail.green
Heart - trail.heart
Lava - trail.lava
Magic - trail.magic
Music - trail.music
Rainbow - trail.rainbow
Spark - trail.spark
Spell - trail.spell
Totem - trail.totem
Water - trail.water
Witch - trail.wmagic
White - trail.white
*************************************************************
Питомцы и права

Пчелка - pets.type.bee
Свинья - pets.type.pig
Кошка - pets.type.cat
Ёж - pets.type.hedgehog
Черепаха - pets.type.turtle
Собака - pets.type.dog
Козел - pets.type.goat
Слон - pets.type.elephant
Хорек - pets.type.ferret
Панда - pets.type.panda
Мышь - pets.type.mouse
Тигр - pets.type.tiger
Лев - pets.type.lion
Лягушка - pets.type.frog
Акула - pets.type.fish
Креветка - pets.type.shrimp
Шалкер - pets.type.shulker
Кролик - pets.type.rabbit
Спавнер - pets.type.spawner
ТНТ - pets.type.tnt

*************************************************************
Превращения и права

Пчела - libsdisguises.disguise.bee
Всплох - libsdisguises.disguise.blaze
Кошка - libsdisguises.disguise.cat
Курица - libsdisguises.disguise.chicken
корова - libsdisguises.disguise.cow
Крипер - libsdisguises.disguise.creeper
Дельфин - libsdisguises.disguise.dolphin
Осел - libsdisguises.disguise.donkey
Эндермен - libsdisguises.disguise.enderman
Вызыватель - libsdisguises.disguise.evoker
Лиса - libsdisguises.disguise.fox
Страж - libsdisguises.disguise.guardian
Лошадь - libsdisguises.disguise.horse
Иллюзор - libsdisguises.disguise.illusioner
Железный Голем - libsdisguises.disguise.iron_golem
Лама - libsdisguises.disguise.llama
Магма куб - libsdisguises.disguise.magma_cube
Грибная корова - libsdisguises.disguise.mushroom_cow
Оцелот - libsdisguises.disguise.ocelot
Панда - libsdisguises.disguise.panda
Попугай - libsdisguises.disguise.parrot
Свино-Зомби - libsdisguises.disguise.pigman
Разбойник - libsdisguises.disguise.pillager
Белый медведь - libsdisguises.disguise.polar_bear
Разоритель - libsdisguises.disguise.ravager
Лошадь-Скелет - libsdisguises.disguise.skeleton_horse
Слизь - libsdisguises.disguise.slime
Снеговик - libsdisguises.disguise.snowman
Паук - libsdisguises.disguise.spider
Спрут - libsdisguises.disguise.squid
Бездомный - libsdisguises.disguise.stray
Черепаха - libsdisguises.disguise.turtle
Сельский житель - libsdisguises.disguise.villager
Блуждающий торговец - libsdisguises.disguise.wandering_trader
Ведьма - libsdisguises.disguise.witch
Визер-скелет - libsdisguises.disguise.wither_skeleton
Волк - libsdisguises.disguise.wolf
Зомби - libsdisguises.disguise.zombie
Зомби лошадь - libsdisguises.disguise.zombie_horse
Сельский житель зомби - libsdisguises.disguise.zombie_villager
Овца - libsdisguises.disguise.sheep
Свинья - libsdisguises.disguise.pig


*************************************************************
Цвета чата и права
Белый - &е - chatcolor.color.white
Серый - &7 - chatcolor.color.gray
Черный - &0 - chatcolor.color.black
Желтый - &е - chatcolor.color.yellow
Золото - &6 - chatcolor.color.gold
Красный - &с - chatcolor.color.red
Темно-красный - &4 - chatcolor.color.darkred
Голубой - &b - chatcolor.color.aqua
Бирюзовый - &3 - chatcolor.color.teal
Синий - &9 - chatcolor.color.blue
Темно-синий - &1 - chatcolor.color.darkblue
Лайм - &a - chatcolor.color.lime
Зеленый - &2 - chatcolor.color.green
Фиолетовый - &d - chatcolor.color.magenta
Пурпурный - &6 - chatcolor.color.purple




*************************************************************
Информация, касающаяся уровней и рангов донаторов
Игрок, впервые зашедший на сервер, получает ранг Default.
У него
есть возможность в игре добавить некоторые способности
получив
уровни развития или приобрести донат для повышения ранга.
Ранги донаторов также могут быть получены через магазин токенов.

*************************************************************
Информация, связанная с уровнями.

Уровни игроков предоставляют дополнительные привилегии.

Используйте следующие команды, чтобы вручную обновить уровень игрока:
/lp user {ник} parent add {уровень}
/lp user {ник} parent remove {предыдущий уровень}
Пример ручного повышения уровня игрока с 1 до 3.
/lp user The_stas parent add 3
/lp user The_stas parent remove 1

ПРИМЕЧАНИЕ: вам не нужно использовать команду удаления, если у игрока нет уровня.

Уровень 1
Требования
Наиграно часов: 10
Стоимость: 50 Изумрудов
Доп. возможности
Доступ к /craft
Доступ к сделкам первого уровня
Дополнительные 50 претензий на землю.
Дополнительная деформация игрока (максимум 2)
Дополнительный дом (2 Макс)

Уровень 2
Требования
Наиграно часов: 20
Стоимость: 50 Изумрудов
Доп. возможности
Доступ к сделкам 2 уровня
Дополнительные 50 претензий на землю.
Дополнительный Деформация Игрока (Макс. 3)
Дополнительный дом (3 Макс)
Уровень 3
Требования
Наиграно часов: 30
Стоимость: 50 Изумрудов
Доп. возможности
Доступ к /enderchest
Доступ к сделкам 3 уровня
Дополнительные 50 претензий на землю.
Дополнительная деформация игрока (4 макс.)
Дополнительный дом (4 макс.)
Уровень 4
Требования
Наиграно часов: 40
Стоимость: 50 Изумрудов
Доп. возможности
Доступ к сделкам 4 уровня.
Дополнительные 50 претензий на землю.
Дополнительный Деформация Игрока (5 Макс.)
Дополнительный дом (5 Макс)
Уровень 5
Требования
Наиграно часов: 50
Стоимость: 50 Изумрудов
Доп. возможности
Доступ к /fly
Доступ к сделкам 5 уровня.
Дополнительные 50 претензий на землю.
Дополнительная деформация игрока (6 макс.)
Дополнительный дом (6 Макс)

*************************************************************

VIP РАНГ
Доступ к /kit vip
Доступ к /ptime
Доступ к /pweather
Доступ к питомцу пчела.
Доступ к маскировке в пчелу.
Доступ к желтому цвету чата.


EVP РАНГ
Доступ к /kit evp
Доступ к /near
Доступ к /hat
Доступ к питомцу лев.
Доступ к золотому цвету чата.


MVP РАНГ
Доступ к /kit mvp
Доступ к /condense
Доступ к /invsee
Доступ к питомцу черепаха.
Доступ к красному цвету чата.
Доступ к цветному шрифту табличек.

*************************************************************
Приведенные, ниже ранги не рекомендуется продавать как донат.
Ранги администрации сервера.

РАНГ ХЕЛПЕР
Максимальная длительность наказания игроков: 7 дней
Доступ к /punish (НИК_ИГРОКА).
Доступ к /tempban
Доступ к /tempmute
Доступ к /tempwarn
Доступ к /socialspy
Доступ к /vanish
Доступ к /god
Доступ к /tpo
Возможность просмотра эндер-сундуков других игроков.
Возможность просмотра инвентаря другого игрока.
Возможность отключить косметику другого игрока.
Получать рудные объявления (анти x-ray).

РАНГ МОДЕРАТОР
Максимальная длительность наказания игроков: 14 дней
Доступ к /banlist
Доступ к /history
Доступ к /change-reason
Доступ к /clearchat
Возможность удалять варпами других игроков.
Возможность изменять эндер-сундуки других игроков.
Возможность изменить инвентарь другого игрока.

РАНГ АДМИНИСТРАТОР
Максимальная длительность наказания игроков: Навсегда
Доступ к /ban
Доступ к /mute
Доступ к /warn
Доступ к /gamemode
Доступ к /announcement
Доступ к /broadcast
Доступ к /warning
Доступ к /mutechat
Возможность управлять регионами игроков.
Возможность управлять магазином игроков.
Возможность принудительного рандомно телепортировать других игроков.


*************************************************************
Информация, связанная с самописным плагином
CUSTOMITEMS
Этот плагин позволяет создать предмет, который дает временные эффекты или эффекты зелья.
Предметы часто используется в кейсах, чтобы предлагать коллекционные предметы, которые дают эффект зелья.
Он был тщательно протестирован, чтобы гарантировать, что временные чары не останутся на предметах.

Чтобы дать игроку пользовательский предмет, введите /customitems give {ник} {id}


*************************************************************

Вопросы и ответы

Информация, связанная с часто задаваемыми вопросами.

Как вручную установить рейтинг игрока? 
Доступные ранги:vip, evp, mvp, helper, mod, admin, owner 
Добавить ранг: /lp user {ник} parent add {ранг}
Удалить ранг: /lp user {ник} parent remove {ранг}




Как вручную установить уровень игроку? 
Доступные уровни: i, ii, iii, iv, v
Добавить уровень: /lp user {ник} parent add {уровень}
Удалить уровень: /lp user {ник} parent remove {старый уровень} 

ПРИМЕЧАНИЕ . Вторая команда не применяется, если у игрока нет уровня.


Как мне вручную дать игроку ключи от кейса? 
Доступные кейсы: basic, rare, ultra, valentine, madness, easter, summer, harrowed, noel
Выдать ключ: /crates give p {кейс} {кол-во} {ник}


Как мне вручную дать токены игроку? 
Добавить токены:/tokenmanager add {ник} {кол-во}
Удалить токены: /tokenmanager remove {ник} {кол-во}


Как мне вручную дать игроку косметику? 
Добавить косметику: /lp user {ник} permission set {право}
Удалить косметику: /lp user {ник} permission unset {право}


ПРИМЕЧАНИЕ: обратитесь к разделу прав для косметических разрешений.


Как отключить объявления о достижениях? 
Введите: /gamerule announceAdvancements false

ПРИМЕЧАНИЕ. Это необходимо сделать для каждого мира, в котором хотите отключить объявления.


Как игрокам сделать магазин?

Шаг 1 - Создание магазина
Ставим табличку на сундук или выше.
Текст в первой строке [Trade].
Закрываем меню таблички.
Это создаст пустой и закрытый магазин.
Теперь решим какой предмет, в каком количестве будем продавать
и за сколько изумрудов.
Например 12 булыжников продадим за один изумруд.

Шаг 2 - Назначение товара на продажу
Берем в руку 12 булыжников.
Смотрите на сундук.
Пишем команду: /ts addProduct

Шаг 3 - Присвоение стоимости
Берем в руку 1 изумруд.
Смотрите на сундук.
Пишем команду: /ts addCost

Шаг 4 - Делаем запас товара и открываем магазин
Как только вы достигли этого шага, все, что вам нужно сделать,
это заполнить сундук с булыжниками и запустить магазин в работу.
Чтобы магазин начал работать наберите /ts open , глядя на сундук.

ПРИМЕЧАНИЕ. Игроки должны использовать /ts open каждый раз,
когда они хотят запустить свой магазин после того, как они обновили его.


Как игроки получают валюту - изумруды?
Квесты, майнинг, охота и торговли.
Игроки могут получить изумруды от убийства мобов,
у мобов 10%-20% шансов дропнуть изумруды.
квесты награждают игроков изумрудами за каждый третий квест,
Изумруды с небольшим шансом выпадают из добытой руды.
Игроки могут создавать фермы для обмена продуктов на изумруды с жителями деревни.
Игроки могут создавать магазины для обмена предметов на изумруды между другими игроками.


*************************************************************

О сборке сервера.
Информация, связанная с настройкой сборки.
Этот сервер выживания, которая предлагает другой вариант с эксклюзивными функциями,
такими как банковское дело, банковские проценты, банковские обновления, аукционы и многое другое.
На сервере отключены игровые монеты. Вся экономика крутится вокруг изумрудов и токенов.

Рекомендуется для владельцев серверов, которые хотели бы расширить свою сеть с помощью игрового режима,
который отличается от традиционного экономичного сервера.
Это привлекательно для игроков, которые предпочли бы другой стиль экономики, а не традиционный экономичный сервер.

ПРИМЕЧАНИЕ. Этот сервер работает на версии 1.15.2! Не рекомендую запускать на более низких версиях.

Плагины (73 шт): 
ActionHealth - Отображение здоровья мобов и игроков - https://www.spigotmc.org/resources/action-bar-health.2661/
AdvancedBan - наказание за нарушение правил сервера - https://www.spigotmc.org/resources/advancedban.8695/
AdvancedPortals - Порталы в другие миры - https://www.spigotmc.org/resources/advanced-portals.14356/
AntiAFK - Защита AFK режима - https://www.spigotmc.org/resources/anti-afk-detects-every-form-of-afk.53889/history
AuthMe - Регистрация и авторизация игроков - https://www.spigotmc.org/resources/authmereloaded.6269/
AutoTool - Автомотический выбор инструмента для добычи блоков - https://www.spigotmc.org/resources/auto-tool.71879/
BetterRTP - Рандомная телепортация - https://www.spigotmc.org/resources/betterrtp.36081/
BlockParticles - Эфекты частиц для блоков - https://www.spigotmc.org/resources/block-particles.13877/
BookNews - Открытие книги при входе на сервер с описанием новостей - https://www.spigotmc.org/resources/booknews-1-8-1-16-1.61163/
ChatColor2 - Цветные сообщения игроков в чате _ https://www.spigotmc.org/resources/chatcolor.22692/
ChatManager - Чат - https://www.spigotmc.org/resources/chat-manager-1-7-1-15-with-30-features-and-40-commands.52245/
ChatReaction - Игра для чата - https://www.spigotmc.org/resources/chatreaction.3748/
ChatReactionPlus - Дополнение для плагина ChatReaction - https://www.spigotmc.org/resources/chatreactionplus.18409/
Citizens - НПС - https://www.spigotmc.org/resources/chatreactionplus.18409/
CitizensCMD - Команды для НПС - https://www.spigotmc.org/resources/citizens-cmd.30224/
CitizensText - Сообщения для НПС - https://www.spigotmc.org/resources/citizenstext.40107/
ClearLag - Очистка мусора - https://www.spigotmc.org/resources/clearlagg.68271/
CombatLogX - Защита режима ПВП - https://www.spigotmc.org/resources/combatlogx.31689/
CrazyCrates - Кейсы - https://www.spigotmc.org/resources/crazy-crates.17599/
CubCustomDrops - Настраиваемый дроп с мобов и блоков - https://www.spigotmc.org/threads/custom-drops-editor-1-13-1-14-1-15-easy-to-use-hard-not-to-want.394961/
CustomItems - Самопис на предметы с временными чарами - https://www.spigotmc.org/members/peaches_mlg.194338/
DamageIndicator - Индикатор нанесенного урона и восстановления - https://www.spigotmc.org/resources/damage-indicator-reborn.55974/
DeluxeCommands - Собственные команды - https://www.spigotmc.org/resources/deluxecommands.8033/
DeluxeMenus - Меню сервера - https://www.spigotmc.org/resources/deluxemenus.11734/
DeluxeTags - Теги для ников - https://www.spigotmc.org/resources/deluxetags.4390/
DivineDrop  - Названия на дропнутых предметах - https://www.spigotmc.org/resources/%E2%98%AF-divinedrop-%E2%98%AF-awesome-drop-management-1-8.51715/
EmeraldBank - Банковская система на изумрудах - https://www.spigotmc.org/resources/the-emerald-bank.71210/
EnchantmentLock - Блокировка зачарований - https://www.spigotmc.org/resources/enchantmentlock.35897/
EntityTrackerFixer - https://www.spigotmc.org/resources/entitytrackerfixer-fix-1-14-4-1-16-1-entitytick-lag.70902/
EssentialsX - Управление сервером - https://www.spigotmc.org/resources/essentialsx.9089/
EssentialsSpawn  - Настройка спавна - https://www.spigotmc.org/resources/essentialsx.9089/
HolographicDisplays - Голограммы - https://dev.bukkit.org/projects/holographic-displays
IllegalStack - Фикс более 50 багов и дюпов - https://www.spigotmc.org/resources/dupe-fixes-illegal-stack-remover.44411/
Images - Собственные картинки - https://www.spigotmc.org/resources/custom-images.53036/
InfiniteScoreboard - Боковое табло - https://www.spigotmc.org/resources/infinite-scoreboard-1-8-8-1-15-x.71482/
InfoHeads - Редактор команд для блоков - https://www.spigotmc.org/resources/infoheads-1-8-1-16-right-click-a-skull-for-information-easy.67080/
InteractionVisualizer - Отображение предметов внутри контейнеров (например в печи) - https://www.spigotmc.org/resources/interactionvisualizer-visualize-function-blocks-like-crafting-tables-with-animations-client-side.77050/
Item2Chat - Показывает в чате другим игрокам характеристики вашего предмета в руке - https://www.spigotmc.org/resources/item2chat.33622/
ItemManager - Редактор предметов - https://www.spigotmc.org/resources/itemmanager.70136/
LightAPI - Освещение местности без факелов, светокамня и так далее - https://www.spigotmc.org/resources/lightapi-fork.48247/
LuckPerms - Менеджер разрешений - https://www.spigotmc.org/resources/luckperms.28140/
NametagEdit - Изменение суффикса и префикса - https://www.spigotmc.org/resources/nametagedit.3836/
PictureLogin - Приветствие игрока в чате с фото его скина - https://www.spigotmc.org/resources/picture-login.4514/
PL-Hide - Скрытие плагинов, защита ОР и многое другое - https://www.spigotmc.org/resources/plugin-hide-1-13-1-16-anti-tabcomplete.68767/
PlaceholderAPI - Заполнители (placeholders) - https://www.spigotmc.org/resources/placeholderapi.6245/
PlayerListPlus - Таб - https://www.spigotmc.org/resources/%E2%99%9B-playerlistdeluxe-%E2%99%9B-1-8-1-15-the-best-tablist-plugin-formerly-playerlistplus.55878/history
PlayerWarps - Варпы игроков с графическим интерфейсом - https://www.spigotmc.org/resources/%E2%AD%90-player-warps-%E2%AD%90-%E2%9E%A2-let-your-players-set-warps-1-8-1-16-1.66692/
PunishmentGUI - Графическим интерфейс для плагина AdvancedBan - https://www.spigotmc.org/resources/%E2%96%B6-punishmentgui-litebansgui-advancedban-more-%E2%97%80-1-8-1-15-fully-customizable.52072/
Quests - Квесты - https://www.spigotmc.org/resources/%E2%96%B6-quests-%E2%97%80-set-up-goals-for-players.23696/updates
ServerListPlus - MOTD - https://www.spigotmc.org/resources/serverlistplus.241/
SimpleRename - Переименование предмета, скрытие атрибутов, изменение цета.... - https://www.spigotmc.org/resources/simple-rename.16220/
SkinsRestorer - Смена скина https://www.spigotmc.org/resources/skinsrestorer.2124/
SmoothSleep - Ускорение ночи - https://www.spigotmc.org/resources/smoothsleep.32043/
SuperTrails - Косметика - https://www.spigotmc.org/resources/supertrails.1879/
SuperVanish - Продвинутый режим ваниш - https://www.spigotmc.org/resources/supervanish-be-invisible.1331/
TokenManager - Токены - https://www.spigotmc.org/resources/tokenmanager.8610/
Trade - Защищенная торговля между игроками - https://www.spigotmc.org/resources/trade-1-15.69680/
TradeShop - Магазины игроков. Обмен товаров на изумруды - https://www.spigotmc.org/resources/tradeshop.32762/
VillagerOptimiser - Оптимизация жителей деревни (нагрузка) - https://www.spigotmc.org/resources/villager-optimiser-1-14-2-1-15-2.68517/
VotingPlugin - Голосование на мониторингах - https://www.spigotmc.org/resources/votingplugin.15358/
WitherAC - Анти-чит - https://www.spigotmc.org/resources/wither-anti-cheat-1-13-x-1-16-x-paper-tuinity-support-free-accurate-optimized-anti-cheat.68657/
LibsDisguises - Превращения в мобов и других игроков - https://www.spigotmc.org/resources/libs-disguises-free.81/
Vault - Вспомогательный плагин - https://dev.bukkit.org/projects/vault
WorldBorder - Ограничение мира - https://www.spigotmc.org/resources/worldborder.60905/
WorldEdit - Редактор мира - https://dev.bukkit.org/projects/worldedit
WorldGuard - Приват и защита регионов = https://dev.bukkit.org/projects/worldguard/
WorldGuardExtraFlags - Дополнительные 29 флагов защиты для регионов - https://www.spigotmc.org/resources/worldguard-extra-flags.4823/
ProtocolLib - Вспомогательный плагин - https://www.spigotmc.org/resources/protocollib.1997/

*************************************************************
Токены являются первостепенной валютой для покупки рангов в игре,
косметики и ключей от кейсов.
Получение токенов
Токены можно заработать в кейсах и квестах.
Выигранная косметика в кейсах, дает определенное
количество токенов
Примечание: Выигранные предметы в кейсах дают
другой ключ кейса вместо токена
Меню Токенов
 /tokens
*************************************************************
Ранги дают доступ к косметике, тэгам к никам,
цветному чату и цветным надписям на табличках.

Покупка  Доната
Донат можно приобрести двумя способами.
Простой - купить на сайте доната.
Сложный - накопить токены и приобрести ранг в игре

Информация о рангах
/ranks или /rank
*************************************************************

За выполнение квестов получите
ключи от кейсов, изумруды и токены.
Пройдите более 240 квестов на разные темы.

Темы квестов
Рыбалка, Охота, Фермерство, Крафт
и многое другое.

Меню квестов
/quests
*************************************************************

Уровни предназначены для обычного
игрока, не имеющего ранга VIP, EVP, MVP.

Особенности
На сервере пять уровней.
Каждый уровень открывает дополнительные
товары в различных магазинах на спавне

Повышение уровня
Покупка уровня за изумруды и игровое время.

Информация о уровнях
/tiers или /tier

*************************************************************

ОБНОВЛЕНИЕ ИНФОРМАЦИИ СЕРВЕРА 
После того, как вы успешно установили свой сервер, вам нужно обновить настройки, чтобы они соответствовали информации вашего сервера.
Ниже приведен список файлов, которые должны быть обновлены с информацией о вашем сервере.

Директория: plugins > deluxecommands > config.yml
Внутри этой конфигурации вы найдете сайт и команду сайта.
Замените https://www.discordapp.com ссылкой на discord вашего сервера.
Замените https://www.website.com ссылкой на веб-сайт вашего сервера.
ПРИМЕЧАНИЕ. Убедитесь, что ваша ссылка содержит https://!

Директория:plugins > deluxemenus > gui_menus > ranks.yml: 
Внутри этой конфигурации вы найдете меню, которое демонстрирует ссылки.
Замените https://store.server.com ссылкой на ваш магазин доната.
ПРИМЕЧАНИЕ. Убедитесь, что ваша ссылка содержит https://!

Директория: plugins > deluxemenus > gui_menus > vote.yml
Внутри этой конфигурации вы найдете меню, которое отправляет игроков на сайты голосования.
Эта меня предполагает, что у вас будет 5 сайтов голосований.
Если вы планируете иметь больше или меньше, вы можете просто удалить ненужные сайты и настроить слоты для новых.
Замените https://www.minecraftservers.org ссылкой на ваш сайт для голосования.
ПРИМЕЧАНИЕ. Не забудьте обновить ссылки каждого сайта голосования.

Директория: plugins > advancedban > messages.yml
Внутри этой конфигурации вы найдете сообщения о наказаниях, которые получают игроки.
Замените server.com/discord ссылкой на discord вашего сервера или веб-сайта.
Замените СЕРВЕР ВЫЖИВАНИЯ на название вашего сервера.

Директория: plugins > chatmanager > config.yml
Внутри этой конфигурации вы найдете сообщения о наказании, которые получают игроки.
Замените СЕРВЕР ВЫЖИВАНИЯ на название вашего сервера.

Директория: plugins > booknews > config.yml
Внутри этой конфигурации вы найдете книгу новостей, которую игроки получают при присоединении.
Замените https://www.discordapp.com/ ссылкой на discord вашего сервера или веб-сайт.
Замените дату и сообщение своими собственными. Будьте внимательны к форматированию.
ПРИМЕЧАНИЕ. Убедитесь, что ваша ссылка содержит https://!

Директория: plugins > serverlistplus > serverlistplus.yml
Внутри этой конфигурации вы найдете сообщения MOTD и Hover.
Замените СЕРВЕР ВЫЖИВАНИЯ на название вашего сервера. Будьте внимательны к форматированию.
ПРИМЕЧАНИЕ. Вы можете использовать /slp reload для обновления MOTD без перезапуска сервера.

Директория: plugins > picturelogin > config.yml
Внутри этой конфигурации вы найдете MOTD присоединения.
Замените СЕРВЕР ВЫЖИВАНИЯ на название вашего сервера. Будьте внимательны к форматированию.
ПРИМЕЧАНИЕ. Вы можете использовать /picturelogin reload для обновления MOTD без перезапуска сервера.

Директория: spigot.yml and bukkit.yml
Внутри этой конфигурации вы найдете сообщения перезапуска/остановки/белого списка, которые получают игроки.
Замените СЕРВЕР ВЫЖИВАНИЯ на название вашего сервера.

Директория: plugins > playerlistplus > config.yml
Внутри этой конфигурации вы найдете вкладку конфигурации.
Замените СЕРВЕР ВЫЖИВАНИЯ на название вашего сервера. Будьте внимательны к форматированию.
ПРИМЕЧАНИЕ. Вы можете использовать /plp reload для обновления без перезагрузки сервера.


*************************************************************



























