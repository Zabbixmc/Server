#!/bin/bash

JAR_FILE="$(find . -maxdepth 1 -name "*.jar")"
FOLDER="$(pwd)"

RESTORE_COLOR='\033[0m'
LIGHTGRAY='\033[01;37m'
RED='\033[01;91m'
GREEN='\033[01;92m'
YELLOW='\033[01;93m'
ORANGE='\033[01;33m'
BLUE='\033[01;34m'
PURPLE='\033[01;35m'
CYAN='\033[01;96m'
WHITE='\033[01;97m'
DARK_CYAN='\033[01;36m'
DARK_GREY='\033[01;90m'
INFO_TEXT_COLOR=${WHITE}

if [[ ! -e '.env' ]]; then
    echo -e "\n\n"
    echo -e "${WHITE}Copying ${GREEN}.env.dist ${WHITE}to ${GREEN}.env\n"

    cp .env.dist .env &> /dev/null

    if [[ $? -eq 0 ]]; then
      echo -e "${WHITE}You can change settings for this project in ${GREEN}.env ${WHITE}file.\n\n"
    else
      echo -e "${ORANGE}Cannot find ${WHITE}.env.dist ${ORANGE}file or cannot copied it to ${WHITE}.env.\n${ORANGE}Will be used default values!${RESTORE_COLOR}\n"
    fi
fi

export $(egrep -v '^#' .env | xargs)

PROJECT_NAME=${PROJECT_NAME:-YOUR PROJECT}
PREFIX="${DARK_GREY}[${CYAN}${PROJECT_NAME}${DARK_GREY}] ${RESTORE_COLOR}"

XMS=${MIN_SIZE:-1024M}
XMX=${MAX_SIZE:-1024M}

echo -e "${RESTORE_COLOR}"

if [[ $(pwd) =~ "bin" ]]; then
    echo -e "\n\n"
    echo -e "${RED}Script cannot be running from ${ORANGE}$(pwd)${RED} folder, please go down one level."
    echo -e "\n\n"
    exit 1
fi

echo -e "${PREFIX} ${INFO_TEXT_COLOR}Server will be running with ${LIGHTGRAY}-Xms${GREEN}${XMS} ${INFO_TEXT_COLOR}and ${LIGHTGRAY}-Xmx${GREEN}${XMX}${INFO_TEXT_COLOR} memory.\n"

echo -e "${RESTORE_COLOR}"

while true
do
{
  {
      rm "${FOLDER}/banned-ips.json"
      rm "${FOLDER}/banned-players.json"
      rm "${FOLDER}/ops.json"
      rm "${FOLDER}/permissions.yml"
  } &> /dev/null

    java -Xms"${XMS}" -Xmx"${XMX}" \
        -XX:+UnlockExperimentalVMOptions \
	-XX:+UseG1GC -XX:+ParallelRefProcEnabled \
        -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions \
        -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 \
        -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 \
        -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 \
        -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 \
        -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 \
        -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 \
        -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true \
        -jar "$FOLDER/$JAR_FILE" -o true nogui

	echo -e "\n"
	echo -e "${PREFIX} ${INFO_TEXT_COLOR}Use ${GREEN}Ctrl${INFO_TEXT_COLOR} + ${GREEN}C ${INFO_TEXT_COLOR}to cancel restart.${RESTORE_COLOR}"
	sleep 1
	for (( i = 0; i < 5; i++ )); do
	echo -e "${PREFIX} ${INFO_TEXT_COLOR}Server stopped. Restarting in ${YELLOW}$((5-i))${INFO_TEXT_COLOR} seconds.${RESTORE_COLOR}"
	sleep 1
	done
	echo -e "\n"
}
done
